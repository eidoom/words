# [words](https://gitlab.com/eidoom/words)

`count_1w.txt` from <https://norvig.com/ngrams/count_1w.txt>

epub to plain text with pandoc:

```sh
pandoc -d pandoc.yml -o output.txt input.epub
```
