#!/usr/bin/env python3

import collections
import re
import sys

if __name__ == "__main__":
    with open(sys.argv[1], "r") as f:
        text = f.read()

    words = re.findall(r"\b[A-Za-z’]+\b", text.lower())
    print(len(words))
    print(len(set(words)))

    # words by how frequently they appear in the text
    freq = collections.Counter(words)  # word -> freq
    qerf = collections.defaultdict(list)  # freq -> words
    for k, v in freq.items():
        qerf[v].append(k)
    qerf = collections.OrderedDict(
        sorted({k: sorted(v) for k, v in qerf.items()}.items(), key=lambda x: x[0])
    )
    print(qerf.keys())

    # words by how long they are
    lett = collections.defaultdict(set)  # word length -> words
    for w in words:
        lett[len(w)].add(w)
    lett = collections.OrderedDict(sorted(lett.items(), key=lambda x: x[0]))
    print(lett.keys())
