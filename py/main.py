#!/usr/bin/env python3

import sys

import wordfreq

if __name__ == "__main__":
    text = sys.stdin.read()

    lang = "en"

    words = wordfreq.tokenize(text, lang)

    rated = [
        (y, wordfreq.word_frequency(y, lang))
        for y in sorted(set(x.split("’")[0] for x in set(words)))
    ]

    filt = True
    if filt:
        rated = [(a, b) for a, b in rated if b]

    ordered = sorted(rated, key=lambda x: x[1])

    for word, _ in ordered:
        print(word)
