module Main where

import Data.Char (isLetter, toLower)
import Data.Containers.ListUtils (nubOrd)
import Data.Either (fromRight)
import Data.List (sortOn)
import qualified Data.Map.Lazy as M
import Data.Maybe (mapMaybe)
import Text.Parsec (eof, parse, skipMany1)
import Text.Parsec.Char (digit, endOfLine, letter, satisfy, spaces)
import Text.Parsec.Combinator (endBy1, many1, optional)
import Text.Parsec.String (Parser)

type Pair = (String, Int)

type Count = M.Map String Int

getWordList :: String -> [String]
getWordList =
  nubOrd
    . filter ((> 4) . length)
    . fromRight []
    . parse parseWords ""
    . map toLower

parseWords :: Parser [String]
parseWords = do
  optional parseIgnore
  wrds <- endBy1 parseWord parseIgnore
  eof
  return wrds

parseIgnore :: Parser ()
parseIgnore = skipMany1 $ satisfy $ not . isLetter

parseWord :: Parser String
parseWord = many1 letter

getFrequencies :: String -> Count
getFrequencies = M.fromList . fromRight [] . parse parsePairs ""

parsePairs :: Parser [Pair]
parsePairs = do
  pairs <- endBy1 parsePair endOfLine
  eof
  return pairs

parsePair :: Parser Pair
parsePair = do
  word <- parseWord
  spaces
  num <- many1 digit
  return (word, read num)

getRank :: Count -> String -> Maybe Pair
getRank ranks x =
  case ranks M.!? x of
    Nothing -> Nothing
    Just y -> Just (x, y)

unusuals :: Count -> [String] -> [String]
unusuals ranks = map fst . sortOn snd . mapMaybe (getRank ranks)

-- print the words which appear least frequently in general English
main :: IO ()
main = do
  ranks <- getFrequencies <$> readFile "../count_1w.txt" -- get from https://norvig.com/ngrams/count_1w.txt
  wordList <- getWordList <$> getContents
  putStrLn $ unwords $ take 600 $ unusuals ranks wordList
