#!/usr/bin/env sh

cat $1 |
  sed "s/[,.—?‘:;\!”]//g" |
  sed "s/’$//" |
  tr " " "\n" |
  tr "-" "\n" |
  tr "’" "\n" |
  tr "[:upper:]" "[:lower:]" |
  sort -u
